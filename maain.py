from collections import defaultdict

from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer(language='french')

import math
import re

import pickle

# Load score_mot_unique from disk
print("loading pre-computed data structures")
with open('score_mot_unique.pickle', 'rb') as f:
    score_mot_unique = pickle.load(f)
print("loading pre-computed data structures done")

# In[118]:


# since stemmer.stem() is a costly function, we store the computed stems into a table to speed up algorithms
stem_table = dict()
def stem(word):
    try:
        return stem_table[word]
    except KeyError:
        stem = stemmer.stem(word)
        stem_table[word] = stem
        return stem


# In[119]:


from spellchecker import SpellChecker
spell = SpellChecker(language='fr')

def correct_spelling(word):
    correction = spell.correction(word)
    if correction is not None and correction != word:
        return correction
    return word


# ## Lecture à partir du disque

# In[120]:


# Lecture du pageRank à partir du disque
def pageRank_read():
    
    pageRank  = defaultdict(str)
    with open('pagerank.txt', 'r') as f:
        for line in f:
            i, value = line.strip().split(': ')
            
            pageRank[i]= float(value)
         
    return pageRank
   

print("loading pagerank from disk")
pageRank = pageRank_read()
print("loading pagerank from disk is done")


# In[122]:


#Lecture de la relation qui associe un titre à l'id d'une page
def id_to_page_read():

    id_to_page = {}
    file_name = 'id_to_page.txt'
    chunk_size = 1000 # Change this value to set the number of lines to read at a time

    with open(file_name, 'r') as f:
        while True:
            lines = f.readlines(chunk_size)
            if not lines:
                break

            # Process the lines here
            for line in lines:
                key, value = line.strip().split(' : ',maxsplit= 1)
                # Do something with the key-value pair
                id_to_page[int(key)] = value

            # Clear memory
            del lines
    return id_to_page
print("loading id_to_page relation from disk")
id_to_page = id_to_page_read()
print("loading id_to_page relation from disk is done")


# In[124]:


def idf_read():
    idf = {}
    with open("idf.txt", "r") as file:
        chunk_size = 1000
        lines = file.readlines(chunk_size)
        while lines:
            for line in lines:
                key_value = line.strip().split(": ")
                idf[key_value[0]] = float(key_value[1])
            lines = file.readlines(chunk_size)
    return idf
print("loading idf structure from disk")
idf = idf_read()
print("loading idf structure from disk is done")


# In[126]:


# Lecture de la relation mot-pages à partir du disque
def mots_pages_read():
    rel_mots_pages = defaultdict(tuple)

    with open("rel_mots-pages.txt", "r") as file:
        chunk_size = 1000
        
        while True:
            lines = file.readlines(chunk_size)
            if not lines:
                break  # end of file

            for line in lines:
                key_value_pairs = line.strip().split(" - ")
                mot = key_value_pairs[0]
                pages = key_value_pairs[1].split("*")
                value_dict = {}

                for page in pages:
                    key_value_pairs2 = page.split(":")
                    if len(key_value_pairs2) >= 2:
                        page_id = int(key_value_pairs2[0])           
                        score = key_value_pairs2[1].strip().replace("(", "").replace(")", "")  
                        score = score.split(",")[0]
                        value_dict[page_id] = float(score)
                
                rel_mots_pages[mot] = value_dict
            
            del lines

    return rel_mots_pages

print("loading words/page relation from disk")
rel_mots_pages = mots_pages_read()
print("loading words/page relation from disk is done")


# ## Traitement de la requête : Version Simple
# 

# In[128]:


def find_pages(r,rel_mots_pages): 
    l = list(rel_mots_pages[r[0]].keys())
    min_len = l[len(l)-1]
    for mot in r:
        l = list(rel_mots_pages[mot].keys())
        end_i = l[len(l)-1]
        if(min_len > end_i) : 
            min_len = end_i
    

    long_req = len(r)

    L = []
    pointers = defaultdict(int)

    for m in r: 
        pointers[m] = 0
        
    while all(p < min_len for p in pointers.values()):
        
        c = 0
        m = max(list(rel_mots_pages[mot].keys())[pointers[mot]] for mot in r)
        
        for mot in r:
            if(pointers[mot] < min_len):
                while( list(rel_mots_pages[mot].keys())[pointers[mot]] < m ):      
                    
                    pointers[mot]+=1
                    c+=1              
                    if(pointers[mot] > len(rel_mots_pages[mot].keys())-1): return L

        if(c==0):
            
            L.append(m)
            for m in r: 
                pointers[m] += 1
                if(pointers[m] > len(rel_mots_pages[m].keys())-1): return L

    return L


# In[129]:


# Retourne f(d, r) le score obtenu par la fréquence des mots
def f(d,r):

    IDF_r = 0

    for word in r:
        IDF_r += (idf[word]*idf[word])
            
    N_r = math.sqrt(IDF_r)

    sum_freq = 0
    for word in r:
        sum_freq += rel_mots_pages[word][d]
        

    score_freq = sum_freq * (1/N_r)


    return score_freq


# In[130]:


# Retourne s(d,r)
def s(d,r):
    
    alpha = 3/7
    beta = 4/7
    
    return alpha*f(d,r)+beta*pageRank[str(d)]      


# In[192]:


def version_simple(r):
    
    ## Pre-traitement de la requete : correction orthographe / stemming / supression redondance dans la requete
    sdr = defaultdict(int)
    r2 = []
    requete = []
    
    r = [correct_spelling(word) for word in r]
    r = [stem(word) for word in r]
    
    for word in r: 
        if word in rel_mots_pages.keys() and word not in requete:
            requete.append(word)  


    if(len(requete) > 0): # si la requete n'est pas vide
    
        liste_pages = find_pages(requete,rel_mots_pages) # On récupere les pages communes à tous les mots de la requête

        for i in range(len(liste_pages)): ## On calcule le score total de chaque page  
            d = liste_pages[i]
            sdr[d] = s(d,requete)


        sdr = [k for k, v in sorted(sdr.items(), key=lambda x: x[1], reverse=True)]


        ## retourner les pages triées par score decroissant  
        return sdr
    else: 
        return []
    


# # Traitement de la requête : Cas d’une requête d’un seul mot


def mot_unique(m):
    m = stem(m)
    return score_mot_unique[m]


# In[145]:


# Nettoyage des pages
def clean_text(text):
    
    if type(text) != str:
        return ""
        
    # Retire les liens externes (balises ref)
    text = re.sub(r"<ref>.*?</ref>",'', text,flags=re.DOTALL)
    text = re.sub(r"<ref.*?>",'', text,flags=re.DOTALL)
    text = re.sub(r"</ref>",'', text)
    text = re.sub(r"<.*?>",'', text)

    # Retire le contenu entre double accolades
    text = re.sub(r"{{([^{}]|{[^{}]*})*}}(?=\.)?", "", text, flags=re.DOTALL)
    text = re.sub(r"{{[^{}|]*\|[^{}]*}}(?=\.)?", "", text, flags=re.DOTALL)
    text = re.sub(r'{{.*?}}', "", text, flags=re.DOTALL)

    # Retire les sections « Notes et références », « Voir aussi », « Bibliographie », « Articles connexes » et « Liens externes »
    text = re.sub(r"(?s)== Notes et références ==.*?(?===|</text>)","", text)
    text = re.sub(r"(?s)== Voir aussi ==.*?(?===|</text>)","", text)
    text = re.sub(r"(?s)=== Bibliographie ===.*?(?====|</text>)","", text)
    text = re.sub(r"(?s)== Bibliographie ==.*?(?===|</text>)","", text)
    text = re.sub(r"(?s)=== Articles connexes ===.*?(?====|</text>)","", text)
    text = re.sub(r"(?s)== Articles connexes ==.*?(?===|</text>)","", text)
    text = re.sub(r"(?s)=== Liens externes ===.*?(?====|</text>)","", text)
    text = re.sub(r"(?s)== Annexes ==.*?(?====|</text>)","", text)
    text = re.sub(r"(?s)</text>.*?(?=</page>)", "</text>\n", text)
    text = re.sub(r"( éd.)","",text)
    text = re.sub(r"<text>","<text",text)

    # Nettoyage espace en trop, ponctuation
    text = re.sub(r"\*[\s\*\.]*\n","", text)
    text = re.sub(r"\*", "", text)
    text = re.sub(r"'+", "'", text)
    text = re.sub(r"===.*?===", "", text)
    text = re.sub(r"==.*?==", "", text)
    text = re.sub(r"=", "", text)
    text = re.sub(r"\"", "", text)
    text = re.sub(r"\b[l|s]'", "", text)
    text = re.sub(r"\[http[^\]]*\]", "", text)  
    
    #text = re.sub(r"\'*'", " ", text)
    text = re.sub(r"\b\w*'(\w+)\b", r'\1', text)
    text = re.sub(r"\==*'", "", text)
    text = re.sub(r"(?<!\=)\=(?!=)", "", text)
    text = re.sub(r'[.,!?;]', '', text)
    
    text = re.sub(r'\{\|.*?\|\}', '', text, flags=re.DOTALL)

    text = re.sub(r"(?<=\n)\.(?=\n)", "", text)
    text = re.sub(r'\n{3,}', '\n\n', text)
    text = re.sub(r"\[\[Fichier:.*?\]\]", "", text, flags=re.DOTALL)
    text = re.sub(r"\[\[File:.*?\]\]", "", text)
    text = re.sub(r"\[\[:Catégorie:.*?\]\]", "", text)
    
    return text.lower()


# In[193]:


def query_to_result(query):
    
    clean_query = clean_text(query)
    words = clean_query.split(" ")
    
    if len(words) == 1:
        res = mot_unique(words[0])[:15]
    else:
        res = version_simple(words)[:15]
    
    return list(map(lambda id : id_to_page[id], res))

print("server is ready")

while True:
    user_query = input("Enter your query: ")
    result = query_to_result(user_query)
    print(result)

